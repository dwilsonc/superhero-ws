package com.in2es.superherows.exception;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.in2es.superherows.exception.dto.ErrorDetails;
import com.in2es.superherows.exception.dto.ErrorDetailsFields;
import com.in2es.superherows.util.Constantes;

/**
 * 
 * Class {@link GlobalExceptionHandler}
 * 
 * @author dwilsonc
 *
 */
@ControllerAdvice
public class GlobalExceptionHandler {
	
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<?> methodArgumentNotValidException(MethodArgumentNotValidException ex) {
		BindingResult result = ex.getBindingResult();
		List<org.springframework.validation.FieldError> fieldErrors = result.getFieldErrors();
		ErrorDetailsFields errorDetails = new ErrorDetailsFields(Constantes.INVALID_PARAMETERS);
		for (org.springframework.validation.FieldError fieldError : fieldErrors) {
			errorDetails.agregarCampoError(fieldError.getDefaultMessage(), fieldError.getField());
		}
		return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(BadRequestException.class)
	public ResponseEntity<?> BadRequestException(Exception ex) {
		ErrorDetails errorDetails = new ErrorDetails(ex.getMessage());
		return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
	}
	
    @ExceptionHandler(value = ValidationException.class)
    public ResponseEntity<?> handleException(ValidationException ex) {
		ErrorDetails errorDetails = new ErrorDetails(ex.getMsg());
		return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
    }
}