package com.in2es.superherows.exception.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class InvalidFields {

	@JsonProperty(value = "message")
	private String message;
	
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty(value = "field-name")
	private String fieldName;
}
