package com.in2es.superherows.exception.dto;


import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ErrorDetailsFields {
	
	@JsonProperty(value = "message")
    private final String message;
	
	@JsonProperty(value = "fields")
    private List<InvalidFields> fields = new ArrayList<>();
	
	public ErrorDetailsFields(String message) {
        this.message = message;
    }
	
	public void agregarCampoError(String mensaje, String nombreCampo) {
    	InvalidFields error = new InvalidFields(mensaje, nombreCampo);
        fields.add(error);
    }

}
