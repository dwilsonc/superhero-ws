package com.in2es.superherows.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(basePackages = "com.in2es.superherows.*")
@EnableJpaRepositories("com.in2es.superherows")
@EntityScan(basePackages = "com.in2es.superherows.*")
public class SuperHeroWsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SuperHeroWsApplication.class, args);
	}
}
