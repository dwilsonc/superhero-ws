package com.in2es.superherows.entity;

import java.io.Serializable;
import java.text.ParseException;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@MappedSuperclass
public class Auditory implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * Campo de auditor�a
	 */
	@Column(name = "CREATION_DATE")
	private Date creationDate;

	/**
	 * Campo de auditor�a
	 */
	@Column(name = "UPDATE_DATE")
	private Date updateDate;
	
	/**
	 * Campo de auditor�a
	 */
	@Column(name = "CREATION_USER")
	private Date creationUser;

	/**
	 * Campo de auditor�a
	 */
	@Column(name = "UPDATE_USER")
	private Date updateUser;
	
	@PrePersist
	protected void onCreate() throws ParseException {
		creationDate = new Date();
	}

	@PreUpdate
	protected void onUpdate() throws ParseException {
		updateDate = new Date();
	}

}

