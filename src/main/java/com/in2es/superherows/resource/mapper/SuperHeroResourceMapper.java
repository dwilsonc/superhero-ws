package com.in2es.superherows.resource.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;

import com.in2es.superherows.dto.SuperHeroDto;
import com.in2es.superherows.resource.SuperHeroRequestResource;
import com.in2es.superherows.resource.SuperHeroResponseResource;

public class SuperHeroResourceMapper {

	public static SuperHeroDto toDto(SuperHeroRequestResource superHeroRequestResource) {
		ModelMapper mapper = new ModelMapper();
		return mapper.map(superHeroRequestResource, SuperHeroDto.class);
	}

	public static SuperHeroResponseResource toResource(SuperHeroDto superHeroDto) {
		ModelMapper mapper = new ModelMapper();
		return mapper.map(superHeroDto, SuperHeroResponseResource.class);
	}

	public static List<SuperHeroResponseResource> toResourceList(List<SuperHeroDto> superHerosDtos) {
		List<SuperHeroResponseResource> listaSuperHerosResource = superHerosDtos.parallelStream().map(c -> {
			SuperHeroResponseResource superHeroResponseResource = new SuperHeroResponseResource();
			superHeroResponseResource.setId(String.valueOf(c.getId()));
			superHeroResponseResource.setName(c.getName());
			return superHeroResponseResource;
		}).collect(Collectors.toList());
		return listaSuperHerosResource;
	}
}
