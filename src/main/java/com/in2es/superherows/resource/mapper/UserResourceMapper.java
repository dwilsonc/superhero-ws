package com.in2es.superherows.resource.mapper;

import org.modelmapper.ModelMapper;

import com.in2es.superherows.dto.UserRequestDto;
import com.in2es.superherows.dto.UserResponseDto;
import com.in2es.superherows.resource.UserRequestResource;
import com.in2es.superherows.resource.UserResponseResource;

public class UserResourceMapper {

	public static UserRequestDto toDto(UserRequestResource userRequestResource) {
		ModelMapper mapper = new ModelMapper();
		return mapper.map(userRequestResource, UserRequestDto.class);
	}

	public static UserResponseResource toResource(UserResponseDto userResponseDto) {
		ModelMapper mapper = new ModelMapper();
		return mapper.map(userResponseDto, UserResponseResource.class);
	}
}
