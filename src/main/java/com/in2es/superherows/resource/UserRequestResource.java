package com.in2es.superherows.resource;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import lombok.Data;

@Data
public class UserRequestResource {

	@NotNull
	@NotBlank
	private String name;

	@NotNull
	@NotBlank
	@Pattern(regexp = "^(.+)@(.+)$", message = "must be a well-formed email address")
	private String email;

	@NotNull
	@NotBlank
	@Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{4,}$", message = "The password must: a lower case letter must occur at least once, "
			+ "an upper case letter must occur at least once, " + "an upper case letter must occur at least once, "
			+ "no whitespace allowed in the entire string, " + "anything, at least four places though")
	private String password;

}
