package com.in2es.superherows.resource;

import lombok.Data;

@Data
public class SuperHeroResponseResource {
	
	private String id;
	
	private String name;

}
