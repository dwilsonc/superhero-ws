package com.in2es.superherows.resource;

import lombok.Data;

@Data
public class UserResponseResource {

	private String id;

	private String created;

	private String modified;
	
	private String lastLogin;

	private String token;
	
	private boolean isActive;

}
