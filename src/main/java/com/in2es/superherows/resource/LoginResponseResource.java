package com.in2es.superherows.resource;

import java.io.Serializable;

import lombok.Data;

@Data
public class LoginResponseResource implements Serializable {

	private static final long serialVersionUID = -8091879091924046844L;

	private final String jwttoken;

}
