package com.in2es.superherows.resource;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class SuperHeroRequestResource {
	
	private String id;
	
	@NotNull
	@NotBlank
	private String name;

}
