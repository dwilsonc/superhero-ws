package com.in2es.superherows.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.in2es.superherows.exception.ValidationException;
import com.in2es.superherows.exception.dto.ErrorDetails;
import com.in2es.superherows.exception.dto.ErrorDetailsFields;
import com.in2es.superherows.resource.SuperHeroRequestResource;
import com.in2es.superherows.resource.SuperHeroResponseResource;
import com.in2es.superherows.resource.UserResponseResource;
import com.in2es.superherows.resource.mapper.SuperHeroResourceMapper;
import com.in2es.superherows.service.SuperHeroService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Class {@link SuperHeroController}
 * 
 * @author dwilsonc
 *
 */
@RestController
@Api(tags = "SuperHero")
public class SuperHeroController {

	@Autowired
	private SuperHeroService superHeroService;

	/**
	 * Method that get all hero
	 * 
	 * @param userRequestResource
	 * @return {@link UserResponseResource}
	 */
	@ApiOperation(value = "SuperHero List", notes = "Service that allows you to search all superheros", response = SuperHeroResponseResource.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Operation Success.", response = SuperHeroResponseResource.class) })
	@GetMapping(value = "/api/v1/superhero")
	public List<SuperHeroResponseResource> getAllSuperHero() {
		return SuperHeroResourceMapper.toResourceList(superHeroService.getAllSuperHero());
	}

	/**
	 * Method that get hero by id
	 * 
	 * @param userRequestResource
	 * @return {@link UserResponseResource}
	 */
	@ApiOperation(value = "Get Superhero", notes = "Service that allows get a superhero by id", response = SuperHeroResponseResource.class)
	@ApiResponses(value = { @ApiResponse(code = 400, message = "Error functional.", response = ErrorDetails.class),
			@ApiResponse(code = 200, message = "Operation Success.", response = SuperHeroResponseResource.class) })
	@GetMapping(value = "/api/v1/superhero/{id}")
	public SuperHeroResponseResource getSuperHeroById(@PathVariable("id") String id) throws ValidationException {

		return SuperHeroResourceMapper.toResource(superHeroService.findById(id));
	}

	/**
	 * Method that get hero by match name
	 * 
	 * @param userRequestResource
	 * @return {@link UserResponseResource}
	 */
	@GetMapping(value = "/api/v1/superhero/like")
	public List<SuperHeroResponseResource> searchHeroByMatchName(@RequestParam(name = "name") String name) {
		return SuperHeroResourceMapper.toResourceList(superHeroService.findByNameLike(name));
	}

	/**
	 * Method that create hero
	 * 
	 * @param userRequestResource
	 * @return {@link UserResponseResource}
	 */
	@ResponseStatus(code = HttpStatus.CREATED)
	@ApiOperation(value = "Create Superhero", notes = "Service that allows create a superhero", response = SuperHeroResponseResource.class)
	@ApiResponses(value = {
			@ApiResponse(code = 201, message = "Operation Success.", response = SuperHeroResponseResource.class),
			@ApiResponse(code = 400, message = "The request could not be interpreted by an invalid syntax.", response = ErrorDetailsFields.class) })
	@PostMapping(value = "/api/v1/superhero")
	public SuperHeroResponseResource createHero(
			@Valid @RequestBody(required = false) SuperHeroRequestResource superHeroRequestResource) {
		return SuperHeroResourceMapper
				.toResource(superHeroService.saveSuperHero(SuperHeroResourceMapper.toDto(superHeroRequestResource)));
	}

	/**
	 * Method that udpdate hero
	 * 
	 * @param userRequestResource
	 * @return {@link UserResponseResource}
	 */
	@ApiOperation(value = "Update Superhero", notes = "Service that allows update a superhero", response = SuperHeroResponseResource.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Error functional.", response = ErrorDetails.class),
			@ApiResponse(code = 200, message = "Operation Success.", response = SuperHeroResponseResource.class),
			@ApiResponse(code = 400, message = "The request could not be interpreted by an invalid syntax.", response = ErrorDetailsFields.class) })
	@PutMapping(value = "/api/v1/superhero/{id}")
	public SuperHeroResponseResource updateHero(
			@ApiParam(value = "Superhero Id", required = true) @PathVariable("id") String id,
			@Valid @RequestBody(required = false) SuperHeroRequestResource superHeroRequestResource) {
		superHeroRequestResource.setId(id);
		return SuperHeroResourceMapper
				.toResource(superHeroService.saveSuperHero(SuperHeroResourceMapper.toDto(superHeroRequestResource)));
	}

	/**
	 * Method that delete hero
	 * 
	 * @param userRequestResource
	 * @return {@link UserResponseResource}
	 */
	@ApiOperation(value = "Delete Superhero", notes = "Service that allows delete a superhero", response = SuperHeroResponseResource.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Error functional.", response = ErrorDetails.class),
			@ApiResponse(code = 200, message = "Operation Success.", response = SuperHeroResponseResource.class) })
	@DeleteMapping(value = "/api/v1/superhero/{id}")
	public String deleteHero(@ApiParam(value = "Superhero Id", required = true) @PathVariable("id") String id) {
		SuperHeroRequestResource superHeroRequestResource = new SuperHeroRequestResource();
		superHeroRequestResource.setId(id);
		superHeroService.deleteHero(SuperHeroResourceMapper.toDto(superHeroRequestResource));
		return "Eliminacion Exitosa!!";
	}

}
