package com.in2es.superherows.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.in2es.superherows.config.JwtToken;
import com.in2es.superherows.exception.BadRequestException;
import com.in2es.superherows.exception.ValidationException;
import com.in2es.superherows.exception.dto.ErrorDetails;
import com.in2es.superherows.exception.dto.ErrorDetailsFields;
import com.in2es.superherows.resource.LoginRequestResource;
import com.in2es.superherows.resource.LoginResponseResource;
import com.in2es.superherows.resource.UserRequestResource;
import com.in2es.superherows.resource.UserResponseResource;
import com.in2es.superherows.resource.mapper.UserResourceMapper;
import com.in2es.superherows.service.UserService;
import com.in2es.superherows.service.impl.UserDetailsServiceImpl;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Class LoginController
 * 
 * @author dwilsonc
 *
 */
@RestController
@Api(tags = "Security")
public class LoginController {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtToken jwtToken;

	@Autowired
	private UserDetailsServiceImpl userDetailsService;

	@Autowired
	private UserService userService;

	/**
	 * Method that allows users to enter
	 * 
	 * @param userDto
	 * @return
	 * @throws BadRequestException
	 */
	@ApiOperation(value = "Login User", notes = "Service that allows login user", response = LoginResponseResource.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Login Success.", response = LoginResponseResource.class),
			@ApiResponse(code = 400, message = "Invalid credentials.", response = ErrorDetails.class) })
	@PostMapping("/api/v1/security/login")
	public ResponseEntity<?> createAuthenticationToken(@RequestBody LoginRequestResource authenticationRequest)
			throws Exception {

		authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());

		final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());

		final String token = jwtToken.generateToken(userDetails);

		userService.updateTokenUsername(token, userDetails.getUsername());
		
		return ResponseEntity.ok(new LoginResponseResource(token));

	}

	/**
	 * Method that create user in BD
	 * 
	 * @param userDto
	 * @return {@link UserResponseResource}
	 * @throws BadRequestException
	 */
	@ResponseStatus(code = HttpStatus.CREATED)
	@ApiOperation(value = "Create Superhero", notes = "Service that allows create a new user", response = UserResponseResource.class)
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Operation Success.", response = UserResponseResource.class),
			@ApiResponse(code = 400, message = "The request could not be interpreted by an invalid syntax.", response = ErrorDetailsFields.class) })
	@PostMapping(value = "/api/v1/security/signup")
	public UserResponseResource signup(@Valid @RequestBody(required = false) UserRequestResource userDto)
			throws BadRequestException {

		if (userDto == null)
			throw new BadRequestException("Body Required");

		if (userService.existsByEmail(userDto.getEmail()))
			throw new ValidationException("Email already existed");

		return UserResourceMapper.toResource(userService.save(UserResourceMapper.toDto(userDto)));
	}

	/**
	 * Method authenticate with spring security
	 * 
	 * @param username
	 * @param password
	 * @throws Exception
	 */
	private void authenticate(String username, String password) throws Exception {

		try {

			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));

		} catch (DisabledException e) {

			throw new ValidationException("User Disabled");

		} catch (BadCredentialsException e) {

			throw new ValidationException("Invalid Credentials!");

		}

	}
}
