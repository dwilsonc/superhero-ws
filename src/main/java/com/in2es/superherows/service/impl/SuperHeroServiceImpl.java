package com.in2es.superherows.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.in2es.superherows.dto.SuperHeroDto;
import com.in2es.superherows.dto.mapper.SuperHeroDtoMapper;
import com.in2es.superherows.entity.SuperHeroEntity;
import com.in2es.superherows.exception.ValidationException;
import com.in2es.superherows.repository.SuperHeroRepository;
import com.in2es.superherows.service.SuperHeroService;

@Service
public class SuperHeroServiceImpl implements SuperHeroService {

	@Autowired
	private SuperHeroRepository superHeroRepository;

	@Override
	public List<SuperHeroDto> getAllSuperHero() {
		return SuperHeroDtoMapper.toDtoList(superHeroRepository.findAll());
	}

	@Override
	public SuperHeroDto saveSuperHero(SuperHeroDto superHeroDto) {
		return SuperHeroDtoMapper.toDto(superHeroRepository.save(SuperHeroDtoMapper.toEntity(superHeroDto)));
	}

	@Override
	public List<SuperHeroDto> findByNameLike(String name) {
		return SuperHeroDtoMapper.toDtoList(superHeroRepository.findByNameLike(name));
	}

	@Override
	public void deleteHero(SuperHeroDto superHeroDto) {
		superHeroRepository.delete(SuperHeroDtoMapper.toEntity(superHeroDto));
	}

	@Override
	public SuperHeroDto findById(String id) throws ValidationException {

		SuperHeroEntity superHeroEntity = superHeroRepository.findById(Integer.parseInt(id))
				.orElseThrow(() -> new ValidationException("No se encontro superhero"));

		return SuperHeroDtoMapper.toDto(superHeroEntity);
	}

}
