package com.in2es.superherows.service.impl;

import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.in2es.superherows.dto.UserDto;
import com.in2es.superherows.dto.UserRequestDto;
import com.in2es.superherows.dto.UserResponseDto;
import com.in2es.superherows.dto.mapper.UserDtoMapper;
import com.in2es.superherows.entity.UserEntity;
import com.in2es.superherows.repository.UserRepository;
import com.in2es.superherows.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public Boolean existsByEmail(String email) {
		return userRepository.existsByEmail(email);
	}

	@Override
	public UserDto findByEmail(String email) {
		return UserDtoMapper.toDto(userRepository.findByEmail(email));
	}

	@Override
	public UserResponseDto save(UserRequestDto userRequestDto) {

		UserResponseDto response = new UserResponseDto();
		UserEntity userEntity = userRepository.findByEmail(userRequestDto.getEmail());	

		if (userEntity != null) {
			userEntity.setEmail(userRequestDto.getEmail());
			userEntity.setName(userRequestDto.getName());
			String encodedPassword = new BCryptPasswordEncoder().encode(userEntity.getPassword());
			userEntity.setPassword(encodedPassword);
			userEntity.setUsername(userEntity.getEmail());
			userEntity = userRepository.save(userEntity);
			response = getResponse(userEntity);
		} else {
			userEntity = UserDtoMapper.toEntity(userRequestDto);
			String encodedPassword = new BCryptPasswordEncoder().encode(userEntity.getPassword());
			userEntity.setPassword(encodedPassword);
			userEntity.setUsername(userEntity.getEmail());
			userEntity.setIsActive(true);
			userEntity = userRepository.save(userEntity);
			response = getResponse(userEntity);
		}

		return response;
	}

	@Override
	public void updateTokenUsername(String token, String username) {
		userRepository.updateTokenUsername(token, username);
	}

	public UserResponseDto getResponse(UserEntity userEntity) {
		UserResponseDto userResponseDto = new UserResponseDto();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		userResponseDto.setId(String.valueOf(userEntity.getId()));
		userResponseDto.setIsActive(userEntity.getIsActive());
		userResponseDto.setToken(userEntity.getToken());
		userResponseDto.setCreated(sdf.format(userEntity.getCreationDate()));

		if (userEntity.getUpdateDate() != null)
			userResponseDto.setModified(sdf.format(userEntity.getUpdateDate()));

		if (userEntity.getLastLogin() != null)
			userResponseDto.setLastLogin(sdf.format(userEntity.getLastLogin()));

		return userResponseDto;
	}
}
