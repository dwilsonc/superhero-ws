package com.in2es.superherows.service;

import com.in2es.superherows.dto.UserDto;
import com.in2es.superherows.dto.UserRequestDto;
import com.in2es.superherows.dto.UserResponseDto;

public interface UserService {
	
	public Boolean existsByEmail(String email);
	
	public UserDto findByEmail(String email);
	
	public UserResponseDto save(UserRequestDto userRequestDto);
		
	public void updateTokenUsername(String token, String username);

}
