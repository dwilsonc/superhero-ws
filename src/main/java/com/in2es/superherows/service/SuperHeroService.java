package com.in2es.superherows.service;

import java.util.List;

import com.in2es.superherows.dto.SuperHeroDto;
import com.in2es.superherows.exception.ValidationException;

public interface SuperHeroService {

	public SuperHeroDto findById(String id) throws ValidationException ;
	
	public List<SuperHeroDto> getAllSuperHero();
	
	public SuperHeroDto saveSuperHero (SuperHeroDto superHeroDto);
	
	public void deleteHero (SuperHeroDto superHeroDto);
	
	List<SuperHeroDto> findByNameLike(String name);
	
}
