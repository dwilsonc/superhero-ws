package com.in2es.superherows.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.in2es.superherows.entity.SuperHeroEntity;

@Repository
public interface SuperHeroRepository extends JpaRepository<SuperHeroEntity, Integer> {
	
	@Query("SELECT m FROM SuperHeroEntity m WHERE LOWER(m.name) LIKE LOWER(CONCAT('%',?1,'%'))")
	List<SuperHeroEntity> findByNameLike(@Param("name") String name);
}
