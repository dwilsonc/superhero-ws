package com.in2es.superherows.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.in2es.superherows.entity.UserEntity;

@Repository
public interface UserRepository extends CrudRepository<UserEntity, Integer> {

	Boolean existsByEmail(String email);

	UserEntity findByEmail(String email);
	
	UserEntity findByUsername(String username);
	
	@Transactional
	@Modifying
	@Query("update UserEntity ue set ue.lastLogin = ?1 where ue.username = ?2")
	public void updateLastLoginUsername(Date lastLogin, String username);

	@Transactional
	@Modifying
	@Query("update UserEntity ue set ue.token = ?1 where ue.username = ?2")
	public void updateTokenUsername(String token, String username);
	
}
