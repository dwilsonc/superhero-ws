package com.in2es.superherows.dto;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuditoryDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private Date creationDate;

	private Date updateDate;
	
	private Date creationUser;

	private Date updateUser;

}
