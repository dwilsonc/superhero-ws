package com.in2es.superherows.dto;

import lombok.Data;

@Data
public class SuperHeroDto {

	private String id;

	private String name;

}
