package com.in2es.superherows.dto;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDto extends AuditoryDto{

	private static final long serialVersionUID = 1L;

	private String id;
	
	private String username;

	private String name;

	private String email;

	private String password;

	private Boolean isActive;
	
	private Date lastLogin;
	
	private String token;
}
