package com.in2es.superherows.dto.mapper;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;

import com.in2es.superherows.dto.SuperHeroDto;
import com.in2es.superherows.entity.SuperHeroEntity;

public class SuperHeroDtoMapper {

	public static SuperHeroEntity toEntity(SuperHeroDto superHeroDto) {
		ModelMapper mapper = new ModelMapper();
		return mapper.map(superHeroDto, SuperHeroEntity.class);
	}
	
	public static SuperHeroDto toDto(SuperHeroEntity superHeroEntity) {
		ModelMapper mapper = new ModelMapper();
		return mapper.map(superHeroEntity, SuperHeroDto.class);
	}
	
	public static List<SuperHeroDto> toDtoList(List<SuperHeroEntity> superHerosEntitys) {
		List<SuperHeroDto> listaSuperHerosDto = superHerosEntitys.parallelStream().map(c -> {
			SuperHeroDto superHeroDto = new SuperHeroDto();
			superHeroDto.setId(String.valueOf(c.getId()));
			superHeroDto.setName(c.getName());
			return superHeroDto;
		}).collect(Collectors.toList());
		return listaSuperHerosDto;
	}
}
