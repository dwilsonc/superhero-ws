package com.in2es.superherows.dto.mapper;

import org.modelmapper.ModelMapper;

import com.in2es.superherows.dto.UserDto;
import com.in2es.superherows.dto.UserRequestDto;
import com.in2es.superherows.entity.UserEntity;

public class UserDtoMapper {

	public static UserEntity toEntity(UserRequestDto userDto) {
		ModelMapper mapper = new ModelMapper();
		return mapper.map(userDto, UserEntity.class);
	}
	
	public static UserEntity toEntity(UserDto userDto) {
		ModelMapper mapper = new ModelMapper();
		return mapper.map(userDto, UserEntity.class);
	}
	
	public static UserDto toDto(UserEntity userEntity) {
		ModelMapper mapper = new ModelMapper();
		return mapper.map(userEntity, UserDto.class);
	}
}
