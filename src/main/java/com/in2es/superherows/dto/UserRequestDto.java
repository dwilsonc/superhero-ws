package com.in2es.superherows.dto;

import lombok.Data;

@Data
public class UserRequestDto {
	
	private String name;

	private String email;

	private String password;

}
