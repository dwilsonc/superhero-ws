package com.in2es.superherows;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import com.in2es.superherows.boot.SuperHeroWsApplication;


@SpringBootTest
@ContextConfiguration(classes=SuperHeroWsApplication.class)
class SuperheroWsApplicationTests {

	@Test
	void contextLoads() {
	}

}
